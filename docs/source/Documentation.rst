Github
############

.. note::
	`Source code of anonym can be found at Github <https://github.com/erdogant/anonym/>`_


Colab Notebook
################

.. note::
	Experiment with ``anonym`` library using the `Colab notebook`_.

.. _Colab notebook: https://colab.research.google.com/github/erdogant/pca/blob/master/notebooks/anonym_notebook.ipynb

