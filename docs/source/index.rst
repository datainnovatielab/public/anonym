anonym's documentation!
========================

|python| |pypi| |gitlab| |downloads_month| |downloads_total| |license| |open issues| |project status|


The ``anonym`` library is designed to anonymize sensitive data in Python, allowing users to work with, share, or publish their data without compromising privacy or violating data protection regulations. It uses Named Entity Recognition (NER) from ``spacy`` to identify sensitive information in the data. Once identified, the library leverages the ``faker`` library to generate fake but realistic replacements. Depending on the type of sensitive information (like names, addresses, dates), corresponding faker methods are used, ensuring the anonymized data maintains a similar structure and format to the original, making it suitable for further data analysis or testing.

-----------------------------------

.. warning::

	**Disclaimer**: While the ``anonym`` library is designed to identify and replace sensitive information, due to the stochastic nature of the Named Entity Recognition (NER) process, there is always a possibility that some names or other privacy-sensitive information may not be identified and replaced. In addition, while certain privacy-senitive information can be faked such as names, one also needs to think that a combination of features may need to be faked. Please review the anonymized data carefully before sharing or publishing.

-----------------------------------

.. code:: bash

	Start
	  |
	  v
	Initialize `anonym` class
	  |
	  v
	Import data using `import_data` method
	  |
	  v
	Anonymize data using `anonymize` method
	  |         |
	  |         v
	  |     Extract entities using `extract_entities` function
	  |         |
	  |         v
	  |     Generate fake labels using `generate_fake_labels` function
	  |         |
	  |         v
	  |     Replace original labels with fake ones using `replace_label_with_fake` function
	  v
	Export anonymized data using `to_csv` method
	  |
	  v
	End


-----------------------------------

.. note::
	**Your ❤️ is important to keep maintaining this package.**
	Report bugs, issues and feature extensions at `github <https://gitlab.com/datainnovatielab/public/anonym/>`_ page.

	.. code-block:: console

	   pip install anonym

-----------------------------------


Content
=======

.. toctree::
   :maxdepth: 1
   :caption: Installation
   
   Installation


.. toctree::
  :maxdepth: 1
  :caption: Methods

  Algorithm
  Import and save


.. toctree::
  :maxdepth: 1
  :caption: Examples

  Examples


.. toctree::
  :maxdepth: 1
  :caption: Documentation
  
  Documentation
  Coding quality
  anonym.anonym



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |python| image:: https://img.shields.io/pypi/pyversions/anonym.svg
    :alt: |Python
    :target: https://img.shields.io/pypi/pyversions/anonym

.. |pypi| image:: https://img.shields.io/pypi/v/anonym.svg
    :alt: |Python Version
    :target: https://pypi.org/project/anonym/

.. |gitlab| image:: https://img.shields.io/badge/Gitlab-Repo-blue.svg
    :alt: Gitlab Repo
    :target: https://gitlab.com/datainnovatielab/public/anonym

.. |downloads_month| image:: https://static.pepy.tech/personalized-badge/anonym?period=month&units=international_system&left_color=grey&right_color=brightgreen&left_text=PyPI%20downloads/month
    :alt: Downloads per month
    :target: https://pepy.tech/project/anonym

.. |downloads_total| image:: https://static.pepy.tech/personalized-badge/anonym?period=total&units=international_system&left_color=grey&right_color=brightgreen&left_text=Downloads
    :alt: Downloads in total
    :target: https://pepy.tech/project/anonym

.. |license| image:: https://img.shields.io/badge/license-MIT-green.svg
    :alt: License
    :target: https://gitlab.com/datainnovatielab/public/anonym/-/blob/main/LICENSE

.. |open issues| image:: https://img.shields.io/badge/issues-you_like-yellow.svg
    :alt: Open Issues
    :target: https://gitlab.com/datainnovatielab/public/anonym/-/issues

.. |project status| image:: http://www.repostatus.org/badges/latest/active.svg
    :alt: Project Status
    :target: http://www.repostatus.org/#active

    

