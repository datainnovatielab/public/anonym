Installation
################

Create environment
**********************

If desired, install ``anonym`` from an isolated Python environment using conda:

.. code-block:: python

    conda create -n env_anonym python=3.10
    conda activate env_anonym


Pypi
**********************

.. code-block:: console

    # Install from Pypi:
    pip install anonym

    # Force update to latest version
    pip install -U anonym


Github source
************************************

.. code-block:: console

    # Install directly from github
    pip install git+https://github.com/erdogant/anonym


Uninstalling
################

Remove environment
**********************

.. code-block:: console

   # List all the active environments. anonym should be listed.
   conda env list

   # Remove the anonym environment
   conda env remove --name anonym

   # List all the active environments. anonym should be absent.
   conda env list


Remove installation
**********************

Note that the removal of the environment will also remove the ``anonym`` installation.

.. code-block:: console

    # Install from Pypi:
    pip uninstall anonym

