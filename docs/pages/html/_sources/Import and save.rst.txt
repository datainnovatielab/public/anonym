
Import and saving
#################

Importing and saving data sets is desired to easily import and share the files.
In order to accomplish this, we created two functions: function :func:`anonym.import_data` and function :func:`anonym.to_csv`
Below we illustrate how to save and load models.


Import from csv
#################

Importing a csv file can be done using the function :func:`anonym.import_data:

.. automodule:: anonym.anonym.anonym.import_data
    :members:
    :undoc-members:


Saving to csv
#################

Saving a data set can be done using the function :func:`anonym.to_csv`:

.. automodule:: anonym.anonym.anonym.to_csv
    :members:
    :undoc-members:
