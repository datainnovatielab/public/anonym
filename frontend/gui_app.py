# Import libraries
import taipy as tp
from taipy import Config, Core, Gui
import pandas as pd
from anonym import anonym
import os
import tempfile


# %% Import some libraries
import datazets as dz
df1 = pd.DataFrame()
df2 = pd.DataFrame()
path = None
path_store = ''
language = 'dutch'  # Default language
languages = ['dutch', 'english']
state_value = ("info", "Click Upload Data")


# %% Build the HTML page
page_1 = """
<|toggle|theme|>
<|navbar|>
<|Settings|expandable|expanded=True|

    <|
<|{language}|selector|lov={languages}|dropdown|hover_text=NER model|> <|{path_store}|input|rebuild|style=width:200%|hover_text=Path to store file|>

<|{path}|file_selector|label=1. Upload|on_action=upload_file|extensions=.csv,.xlsx|drop_message=Drop Message|>
<|button|label=2. Anonymize|on_action=run_anonym|>
<|button|label=3. Download|on_action=download_file|name=filename|hover_text=Download file|>
<|{state_value}|status|>
    |>
|>


    <|
<|layout|columns=1 1|

<|{df1}|table|rebuild|width=99%|>

<|{df2}|table|rebuild|width=99%|>
    |>

|>
"""

page_2 = """
<|navbar|>

"""


# %% upload file
def upload_file(state):
    # Let op: state is het object waar automatisch variabelen uit gepakt wordt in de front-end
    # path is gekoppeld aan {path} die ook altijd bij start geinitialiseerd moet worden.
    # Door df2 op te slaan in state, kan het daarna direct gebruikt worden in de front-end
    print(f"'path' was changed to: {state.path}")
    state.df1 = pd.read_csv(state.path, delimiter=';', encoding='latin-1')
    # Store the path in the input field
    state.path_store = state.path[:-4:] + '_anonym' + state.path[-4:]
    print(f"'path' was changed to: {state.path_store}")
    # Update status
    state.state_value = ("success", "Uploaded done")
    # print(df1)


# upload file
def download_file(state):
    print('Store to disk [%s]' %(state.path_store))
    if os.path.isfile(state.path_store):
        state.state_value = ("error", "Overwrite not allowed")
    else:
        state.state_value = ("warning", "Start creating export")
        # Get the system's temporary directory path
        # temp_dir = tempfile.gettempdir()
        # Define the file path in the temporary directory
        # file_path = os.path.join(temp_dir, 'data_fake.csv')
        state.df2.to_csv(state.path_store, index=False)
        # state.path_download = file_path
        # print(file_path)
        state.state_value = ("success", "Download done")
        # download(state, content=temp_file.name, name="pi.csv", on_action=clean_up)


# Remove the temporary file
def clean_up(state):
    os.remove(state.temp_path)


def run_anonym(state):
    print('Anonymize data..')
    state.state_value = ("warning", "Start anonymizing")
    # print(state.language)
    # Initialize the model
    model = anonym(language=state.language, verbose='info')
    # Run anonymize with input data set
    state.df2 = model.anonymize(state.df1)
    state.state_value = ("success", "Data anonymized")
    # print(state.df2)


# %% Main
if __name__ == "__main__":
    pages = {
        "Settings": page_1,
        "Layout": page_2,
        }
    gui = Gui(pages=pages)
    gui.run(dark_mode=False)
