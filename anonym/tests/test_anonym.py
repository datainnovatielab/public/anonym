import unittest
import anonym as anonym

class Testanonym(unittest.TestCase):
    def test_extract_entities_for_string(self):
        nlp = anonym.nlp
        text = "John Doe lives in New York"
        NER_blacklist = []
        result = anonym.extract_entities_for_string(nlp, text, NER_blacklist)
        self.assertIsNotNone(result)

    def test_anonym_function(self):
        col = "John Doe lives in New York"
        do_not_fake = []
        nlp = anonym.nlp
        labels = []
        NER_blacklist = []
        result = anonym.anonym_function(col, do_not_fake, nlp, labels, NER_blacklist)
        self.assertIsNotNone(result)